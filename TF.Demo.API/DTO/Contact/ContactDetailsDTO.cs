﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TF.Demo.API.DTO.Phones;

namespace TF.Demo.API.DTO.Contact
{
    public class ContactDetailsDTO
    {
        public int Id { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public DateTime? BirthDate { get; set; }
        public string Email { get; set; }
        public IEnumerable<PhoneDTO> Phones { get; set; }
    }
}
