﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TF.Demo.API.DTO.Contact
{
    public class ContactAddDTO
    {
        [Required]
        [MaxLength(50)]
        public string LastName { get; set; }

        [Required]
        [MaxLength(50)]
        public string FirstName { get; set; }

        public DateTime? BirthDate { get; set; }

        [Required]
        [MaxLength(255)]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [MaxLength(25)]
        public string PhoneType { get; set; }

        [Required]
        [MaxLength(25)]
        public string PhoneNumber { get; set; }
    }
}
