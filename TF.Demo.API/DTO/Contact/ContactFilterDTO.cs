﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TF.Demo.API.DTO.Contact
{
    public class ContactFilterDTO
    {
        public string Keyword { get; set; }

        [Range(10,100)]
        public int Limit { get; set; } = 10;
        public int Offset { get; set; }
    }
}
