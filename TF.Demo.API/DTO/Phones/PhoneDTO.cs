﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TF.Demo.API.DTO.Phones
{
    public class PhoneDTO
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public string Number { get; set; }
    }
}
