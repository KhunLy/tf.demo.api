﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using TF.Demo.API.DTO.Contact;
using TF.Demo.API.DTO.Phones;
using TF.Demo.DAL;
using TF.Demo.DAL.Entities;
using ToolBox.Mapper;

namespace TF.Demo.API.Services
{
    public class ContactService : IContactService
    {
        private readonly ContactContext _dc;

        public ContactService(ContactContext dc)
        {
            _dc = dc;
        }

        public IEnumerable<ContactDTO> GetContacts(ContactFilterDTO filter)
        {
            return _dc.Contacts
                .Where(c => filter.Keyword == null || c.LastName.StartsWith(filter.Keyword) || c.FirstName.StartsWith(filter.Keyword))
                .Skip(filter.Offset)
                .Take(filter.Limit)
                .Select(c => c.Map<ContactDTO>());
        }

        public ContactDetailsDTO GetOneContact(int id)
        {
            Contact c = _dc.Contacts.Include(c => c.Phones)
                .FirstOrDefault(c => c.Id == id);
            //Contact c1 = _dc.Contacts.Include(c => c.Phones).Find(id);
            //_dc.Contacts.Where(c => c.Id == id).FirstOrDefault();
            ContactDetailsDTO dto = c?.Map<ContactDetailsDTO>();
            if(dto != null)
                dto.Phones = c.Phones.Select(p => p.Map<PhoneDTO>());
            return dto;
        }

        public void AddContact(ContactAddDTO form)
        {
            using TransactionScope scope = new();
            try
            {
                Contact saved = _dc.Contacts.FirstOrDefault(c => c.LastName == form.LastName && c.FirstName == form.FirstName);
                if (saved is null)
                {
                    // ajouter un contact dans la table contact ?  quid du tél ?
                    saved = _dc.Contacts.Add(form.Map<Contact>()).Entity;
                    _dc.SaveChanges();
                }
                _dc.Phones.Add(new Phone
                {
                    ContactId = saved.Id,
                    Number = form.PhoneNumber,
                    Type = form.PhoneType
                });
                _dc.SaveChanges();
                scope.Complete();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool Update(int id, ContactEditDTO form)
        {
            //Contact toUpdate = form.Map<Contact>();
            //toUpdate.Id = id;
            //_dc.Entry(toUpdate);
            Contact toUpdate = _dc.Contacts.Find(id);
            if (toUpdate is null) return false;
            form.MapToInstance(toUpdate);
            _dc.SaveChanges();
            return true;
        }

        public bool Delete(int id)
        {
            Contact c = _dc.Contacts.Find(id);
            if (c is null) return false;
            _dc.Contacts.Remove(c);
            _dc.SaveChanges();
            return true;
        }
    }
}
