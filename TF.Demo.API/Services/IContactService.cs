﻿using System.Collections.Generic;
using TF.Demo.API.DTO.Contact;

namespace TF.Demo.API.Services
{
    public interface IContactService
    {
        void AddContact(ContactAddDTO form);
        bool Delete(int id);
        IEnumerable<ContactDTO> GetContacts(ContactFilterDTO filter);
        ContactDetailsDTO GetOneContact(int id);
        bool Update(int id, ContactEditDTO form);
    }
}