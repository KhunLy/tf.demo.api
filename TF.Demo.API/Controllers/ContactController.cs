﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TF.Demo.API.DTO.Contact;
using TF.Demo.API.Services;
using TF.Demo.DAL;
using TF.Demo.DAL.Entities;

namespace TF.Demo.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContactController : ControllerBase
    {
        private readonly IContactService _contactService;

        public ContactController(IContactService contactService)
        {
            _contactService = contactService;
        }

        [HttpGet]
        [Produces(typeof(IEnumerable<ContactDTO>))] // documenter l'api
        public IActionResult Get([FromQuery]ContactFilterDTO filter)
        {
            try
            {
                return Ok(_contactService.GetContacts(filter));
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }

        [HttpGet("{id}")]
        [Produces(typeof(IEnumerable<ContactDetailsDTO>))]
        public IActionResult Get([FromRoute] int id)
        {
            try
            {
                ContactDetailsDTO result = _contactService.GetOneContact(id);
                if (result is null) return NotFound();
                return Ok(result);
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }


        [HttpPost]
        public IActionResult Post([FromBody] ContactAddDTO form)
        {
            try
            {
                _contactService.AddContact(form);
                return NoContent();
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }

        [HttpPut("{id}")]
        public IActionResult Put([FromRoute] int id, [FromBody] ContactEditDTO form)
        {
            try
            {
                if (_contactService.Update(id, form))
                    return NoContent();
                return BadRequest();
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete([FromRoute] int id)
        {
            try
            {
                if( _contactService.Delete(id))
                    return NoContent();
                return BadRequest();
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }
    }
}
