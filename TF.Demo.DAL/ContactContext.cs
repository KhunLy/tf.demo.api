﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TF.Demo.DAL.Configs;
using TF.Demo.DAL.Entities;

namespace TF.Demo.DAL
{
    public class ContactContext : DbContext
    {
        //private readonly string _connectionString 
        //    //= "server=K-LAPTOP;initial catalog=gestContact;uid=sa;pwd=test1234";
        //    = "server=K-PC;initial catalog=gestContact;integrated security=true";
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<Phone> Phones { get; set; }

        public ContactContext(DbContextOptions options)
            : base(options)
        {

        }

        //protected override void OnConfiguring(DbContextOptionsBuilder options)
        //{
        //    //options.UseSqlServer(_connectionString);

        //}

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new ContactConfig());
            builder.ApplyConfiguration(new PhoneConfig());
        }
    }
}
