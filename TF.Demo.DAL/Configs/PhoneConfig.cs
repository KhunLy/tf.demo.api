﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TF.Demo.DAL.Entities;

namespace TF.Demo.DAL.Configs
{
    class PhoneConfig : IEntityTypeConfiguration<Phone>
    {
        public void Configure(EntityTypeBuilder<Phone> builder)
        {
            builder.Property(p => p.Number)
                .IsRequired()
                .HasMaxLength(25)
                .IsUnicode(false);

            builder.Property(p => p.Type)
                .IsRequired()
                .HasMaxLength(25);

            builder.HasIndex(p => new { p.Number, p.ContactId }).IsUnique();

            builder.HasOne(p => p.Contact).WithMany(c => c.Phones)
                .HasForeignKey(p => p.ContactId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
