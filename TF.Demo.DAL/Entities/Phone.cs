﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TF.Demo.DAL.Entities
{
    public class Phone
    {
        public int Id { get; set; }
        public int ContactId { get; set; }
        public string Type { get; set; }
        public string Number { get; set; }
        public virtual Contact Contact { get; set; }
    }
}
